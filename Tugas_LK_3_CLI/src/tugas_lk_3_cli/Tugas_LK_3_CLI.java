/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugas_lk_3_cli;

import MyComputer.Komputer;


/**
 *
 * @author herryprasetyo
 */
public class Tugas_LK_3_CLI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Komputer pc = new Komputer();
        //getHDD
        System.out.println("Spesifikasi Komputer Saya :");
        String hdd = pc.getHdd();
        System.out.println("HDD  = " + hdd);
        
        //getRam 
        
        String ram = pc.getRam();
        System.out.println("RAM saya =" + ram);
        
        //getProcessor
        String processor = pc.getProcessor();
        System.out.println("Processor Saya = " + processor);
       
    }
    
}
