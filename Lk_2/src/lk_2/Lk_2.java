/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk_2;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author herryprasetyo
 */
public class Lk_2 extends JFrame {
    private Container panel = new Container();
    public Lk_2(String title){
        super("Latihan Herry Prasetyo");
        setSize(400, 350);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle(title);
        //tambahan komponen
        panel = getContentPane();
        JButton tombolA = new JButton("Herry Prasetyo(SMKN 1 Gunung Putri)");
        JButton tombolB = new JButton("Left)");
        JButton tombolC = new JButton(new ImageIcon("/home/herryprasetyo/NetBeansProjects/Lk_2/src/images/1.jpeg"));
        
        JButton tombolD = new JButton("Kanan");
        JButton tombolE = new JButton("Bawah");
        
//        panel.add(tombolA);
//        panel.add(tombolB);
//        panel.add(tombolC);
//        panel.add(tombolD);
//        panel.add(tombolE);
//        
//        add(panel);
        
        //Atur Layout 
//        panel.setLayout(new BorderLayout());
        panel.add(tombolA, BorderLayout.NORTH);
        panel.add(tombolB, BorderLayout.WEST);
        panel.add(tombolC, BorderLayout.CENTER);
        panel.add(tombolD, BorderLayout.EAST);
        panel.add(tombolE, BorderLayout.SOUTH);
        
        
        //menampilkan Jendela 
        setVisible(true);
        
        //Tengahkan Jendela diTengah Laayar
        setLocationRelativeTo(null);
        
    }

    public static void main(String[] args) {
        JFrame.setDefaultLookAndFeelDecorated(true);
        new Lk_2("Tugas LK 2");
    }

    

    /**
     * @param args the command line arguments
     */
  
    
}
