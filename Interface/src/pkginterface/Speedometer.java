/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkginterface;

/**
 *
 * @author herryprasetyo
 */
public class Speedometer {
    
}

class Mobil extends Speedometer{
    public void tambahKecepatan(){
        System.out.println("Injak Kopling lalu, pindah ke gear yang lebih tinggi Dan Gas Mobilnya");
    }
    public void kurangiKecepatan(){
        System.out.println("Rem Mobilnya dan pindah gear yang lebih rendah");
    }
}

class Motor extends Speedometer{
    public void tambahKecepatan(){
        System.out.println("pindah ke gear yang lebih tinggi dan Gas Motornya");
    }
    public void kurangiKecepatan(){
        System.out.println("rem motornya dengan rem belakang + depan, lalu pindah gear yang lebih rendah");
    }
}

class TestKendaraan{
    public static void main(String[] args){
        Mobil mobil = new Mobil(); 
        Motor motor = new Motor(); 
        
        System.out.println("=============MOTOR==============");
        
        System.out.println("Cara Mengebut pake motor");
        motor.tambahKecepatan();
        
        
        
        System.out.println("Cara berhentinya"); 
        motor.kurangiKecepatan();
        
        System.out.println("=============Mobil==============");
        System.out.println("Cara balapan pake mobil");
        mobil.tambahKecepatan();
        
        System.out.println("KLo Sudah Puas");
        mobil.kurangiKecepatan();
    }
}
